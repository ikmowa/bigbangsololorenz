package be.sweetmustard.internship.soloprojects.bigbangsolo.service;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Serie;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SerieService {
    Optional<Serie> findById(UUID id);

    List<Serie> findAllSeries();

    Serie save(Serie serie);

    Serie update(Serie serie);

    void delete(UUID id);
}
