package be.sweetmustard.internship.soloprojects.bigbangsolo.service.exceptions;

public class PersonServiceException extends RuntimeException {
    public PersonServiceException() {
    }

    public PersonServiceException(String message) {
        super(message);
    }

    public PersonServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersonServiceException(Throwable cause) {
        super(cause);
    }

    protected PersonServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
