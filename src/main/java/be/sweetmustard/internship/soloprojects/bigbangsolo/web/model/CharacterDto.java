package be.sweetmustard.internship.soloprojects.bigbangsolo.web.model;

import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.validation.PostValidation;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.validation.PutValidation;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

public class CharacterDto {
    private PersonDto personDto;

    @NotNull(groups = PutValidation.class)
    @Null(groups = PostValidation.class)
    private String partId;

    @NotBlank
    private String name;

    @NotBlank
    private String bio;

    @NotNull
    @Min(1)
    private Integer startingSeason;

    public PersonDto getPersonDto() {
        return personDto;
    }

    public void setPersonDto(PersonDto personDto) {
        this.personDto = personDto;
    }

    public String getPartId() {
        return partId;
    }

    public void setPartId(String partId) {
        this.partId = partId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Integer getStartingSeason() {
        return startingSeason;
    }

    public void setStartingSeason(Integer startingSeason) {
        this.startingSeason = startingSeason;
    }
}
