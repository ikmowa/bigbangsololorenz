package be.sweetmustard.internship.soloprojects.bigbangsolo.service.exceptions;

public class PartServiceException extends RuntimeException {
    public PartServiceException(String s) {
    }

    public PartServiceException() {
        super();
    }

    public PartServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PartServiceException(Throwable cause) {
        super(cause);
    }

    protected PartServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
