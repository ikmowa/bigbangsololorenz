package be.sweetmustard.internship.soloprojects.bigbangsolo.web.model;

import javax.validation.constraints.NotBlank;

public class PersonDto extends AbstractBigBangDto{
    @NotBlank
    private String name;

    @NotBlank
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
