package be.sweetmustard.internship.soloprojects.bigbangsolo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BigbangsoloApplication {
    public static void main(String[] args) {
        SpringApplication.run(BigbangsoloApplication.class, args);
    }
}
