package be.sweetmustard.internship.soloprojects.bigbangsolo.repositories;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Part;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PartRepository extends JpaRepository<Part, UUID> {
    List<Part> findAllBySerieIdOrderByName(final UUID serieId);
}
