package be.sweetmustard.internship.soloprojects.bigbangsolo.web.controllers.mappers;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Person;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.PersonDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper(uses = UUIDMapper.class)
public interface PersonMapper {
    PersonDto mapPersonToDto(Person person);

    List<PersonDto> mapPersonsToDtos(List<Person> persons);

    Person mapDtoToPerson(PersonDto dto);
}
