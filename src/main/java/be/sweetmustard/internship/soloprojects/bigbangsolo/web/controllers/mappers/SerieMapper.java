package be.sweetmustard.internship.soloprojects.bigbangsolo.web.controllers.mappers;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Serie;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.SerieDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(uses = UUIDMapper.class)
public interface SerieMapper {
    SerieDto mapSerieToDto(Serie serie);
    List<SerieDto> mapSeriesToDtos(List<Serie> series);
    Serie mapDtoToSerie(SerieDto serieDto);
}
