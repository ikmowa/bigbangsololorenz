package be.sweetmustard.internship.soloprojects.bigbangsolo.web.controllers;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Part;
import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Person;
import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Serie;
import be.sweetmustard.internship.soloprojects.bigbangsolo.service.PartService;
import be.sweetmustard.internship.soloprojects.bigbangsolo.service.PersonService;
import be.sweetmustard.internship.soloprojects.bigbangsolo.service.SerieService;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.controllers.mappers.CharacterMapper;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.CharacterDto;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.validation.PostValidation;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.validation.PutValidation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/serie/{serieId}/part")
public class SeriePartRestController {

    private final CharacterMapper mapper;
    private final PartService partService;
    private final SerieService serieService;
    private final PersonService personService;

    public SeriePartRestController(final CharacterMapper mapper, final PartService partService, final SerieService serieService, final PersonService personService) {
        this.mapper = mapper;
        this.partService = partService;
        this.serieService = serieService;
        this.personService = personService;
    }

    @GetMapping(value = "/")
    public List<CharacterDto> getCharactersBySerie(@PathVariable("serieId") final UUID serieId) {
        return mapper.mapPartsToCharacterDtos(partService.findAllBySerieId(serieId));
    }

    @PostMapping(value = "/")
    public ResponseEntity<CharacterDto> postCharacterOnSerie(@RequestBody @Validated(PostValidation.class) final CharacterDto characterDto, @PathVariable("serieId") final UUID serieId) {
        Part part = getPartBasedOnCharacterDto(characterDto,serieId);
        CharacterDto dto = mapper.mapPartToCharacterDto(partService.save(part));
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }
    //Use linked serie & person to put part on respective dto for Post- & Putmapping
    private Part getPartBasedOnCharacterDto(final CharacterDto characterDto, final UUID serieId) {
        Part part = mapper.mapCharacterDtoToPart(characterDto);
        setSerieByIdOnPart(serieId, part);
        setPersonByIdOnPart(UUID.fromString(characterDto.getPersonDto().getId()), part);
        return part;
    }
    //link person to respective part
    private void setPersonByIdOnPart (final UUID personId, final Part part) {
        Person person = personService.findPersonById(personId).orElseThrow(IllegalStateException::new);
        part.setPerson(person);
    }
    //link serie to respective part
    private void setSerieByIdOnPart (final UUID serieId, final Part part) {
        Serie serie = serieService.findById(serieId).orElseThrow(IllegalStateException::new);
        part.setSerie(serie);
    }

    @PutMapping(value = "/")
    public ResponseEntity<CharacterDto> putCharacterOnSerie(@RequestBody @Validated(PutValidation.class) final CharacterDto characterDto, @PathVariable("serieId") final UUID serieId) {
        Part part = getPartBasedOnCharacterDto(characterDto, serieId);
        return new ResponseEntity<>(mapper.mapPartToCharacterDto(partService.update(part)), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{partId}")
    public ResponseEntity deleteCharacterByPartId(@PathVariable("serieId") final UUID serieId, @PathVariable("partId") final UUID partId) {
        Part part = partService.findById(partId).orElseThrow(IllegalStateException::new);
        if (part.getSerie().getId().equals(serieId)) {
            partService.deleteById(partId);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        } else {
            throw new IllegalStateException("Could not remove the given part, as it does not belong to the given serie");
        }
    }
}
