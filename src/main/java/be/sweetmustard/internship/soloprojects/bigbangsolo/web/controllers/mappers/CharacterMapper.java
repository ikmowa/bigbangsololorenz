package be.sweetmustard.internship.soloprojects.bigbangsolo.web.controllers.mappers;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Part;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.CharacterDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(uses = {UUIDMapper.class, PersonMapper.class})
public interface CharacterMapper {

    List<CharacterDto> mapPartsToCharacterDtos(List<Part> parts);

    @Mapping(source = "partId", target = "id")
    Part mapCharacterDtoToPart(CharacterDto characterDto);

    @Mapping(source = "id", target = "partId")
    CharacterDto mapPartToCharacterDto(Part part);
}
