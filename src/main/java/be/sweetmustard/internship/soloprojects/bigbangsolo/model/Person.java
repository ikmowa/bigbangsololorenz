package be.sweetmustard.internship.soloprojects.bigbangsolo.model;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
public class Person extends AbstractBigbangEntity {
    private String name;
    private String url;
    private Boolean active;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "person")
    private Set<Part> parts;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<Part> getParts() {
        return parts;
    }

    public void setParts(Set<Part> parts) {
        this.parts = parts;
    }
}
