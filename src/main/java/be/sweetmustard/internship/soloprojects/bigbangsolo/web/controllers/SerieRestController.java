package be.sweetmustard.internship.soloprojects.bigbangsolo.web.controllers;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Serie;
import be.sweetmustard.internship.soloprojects.bigbangsolo.service.SerieService;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.controllers.mappers.SerieMapper;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.SerieDto;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.validation.PostValidation;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.validation.PutValidation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/serie")
public class SerieRestController {

    private final SerieService service;
    private final SerieMapper mapper;

    public SerieRestController(final SerieService service, final SerieMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<SerieDto> getSerie(@PathVariable("id") final UUID id) {
        return service.findById(id)
                .map(mapper::mapSerieToDto)
                .map(dto -> new ResponseEntity<>(dto, HttpStatus.OK))
                .orElseGet(()-> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "/")
    public List<SerieDto> getAllSeries() {
        return mapper.mapSeriesToDtos(service.findAllSeries());
    }

    @PostMapping(value = "/")
    public ResponseEntity<SerieDto> postSerie(@RequestBody @Validated(PostValidation.class) final SerieDto serieDto) {
        Serie saved = service.save(mapper.mapDtoToSerie(serieDto));
        return new ResponseEntity<>(mapper.mapSerieToDto(saved), HttpStatus.CREATED);
    }

    @PutMapping(value = "/")
    public ResponseEntity<SerieDto> putSerie(@RequestBody @Validated(PutValidation.class) final SerieDto serieDto) {
        Serie saved = service.update(mapper.mapDtoToSerie(serieDto));
        return new ResponseEntity<>(mapper.mapSerieToDto(saved), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteSerie(@PathVariable("id") final UUID id) {
        service.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
