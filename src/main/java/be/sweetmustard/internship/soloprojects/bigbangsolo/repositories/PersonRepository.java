package be.sweetmustard.internship.soloprojects.bigbangsolo.repositories;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PersonRepository extends JpaRepository<Person, UUID> {
    Optional<Person> findByIdAndActiveIsTrue(UUID id);

    List<Person> findAllByActiveIsTrue();
}
