package be.sweetmustard.internship.soloprojects.bigbangsolo.service;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Part;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PartService {
    List<Part> findAllBySerieId(UUID serieId);

    Part save(Part part);

    Part update(Part part);

    Optional<Part> findById(UUID id);

    void deleteById(UUID id);
}
