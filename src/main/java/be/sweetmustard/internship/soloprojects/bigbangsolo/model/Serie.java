package be.sweetmustard.internship.soloprojects.bigbangsolo.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
public class Serie extends AbstractBigbangEntity{
    @Column(nullable = false)
    private String name;

    private String url;

    @Column(nullable = false)
    private LocalDate startDate;

    private LocalDate endDate;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "serie")
    private Set<Part> parts;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Set<Part> getParts() {
        return parts;
    }

    public void setParts(Set<Part> parts) {
        this.parts = parts;
    }
}
