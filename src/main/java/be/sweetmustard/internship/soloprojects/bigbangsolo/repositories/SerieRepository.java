package be.sweetmustard.internship.soloprojects.bigbangsolo.repositories;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Serie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SerieRepository extends JpaRepository<Serie, UUID> {
}
