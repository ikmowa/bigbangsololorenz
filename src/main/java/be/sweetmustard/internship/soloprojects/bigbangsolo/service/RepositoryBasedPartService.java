package be.sweetmustard.internship.soloprojects.bigbangsolo.service;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Part;
import be.sweetmustard.internship.soloprojects.bigbangsolo.repositories.PartRepository;
import be.sweetmustard.internship.soloprojects.bigbangsolo.service.exceptions.PartServiceException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RepositoryBasedPartService implements PartService {
    private final PartRepository repository;

    public RepositoryBasedPartService(final PartRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Part> findAllBySerieId(final UUID serieId) {
        return repository.findAllBySerieIdOrderByName(serieId) ;
    }

    @Override
    public Part save(final Part part) {
        return repository.save(part);
    }

    @Override
    public Part update(final Part part) {
        Optional<Part> perhapsAPart = repository.findById(part.getId());
        if (perhapsAPart.isPresent()) {
            return repository.save(part);
        } else {
            throw new PartServiceException("The Part you tried to update does not exist");
        }
    }

    @Override
    public Optional<Part> findById(final UUID id) {
        return repository.findById(id);
    }

    @Override
    public void deleteById(final UUID id) {
        Optional<Part> perhapsAPart = repository.findById(id);
        if (perhapsAPart.isPresent()) {
            repository.delete(perhapsAPart.get());
        } else {
            throw new PartServiceException("The Part you tried to delete does not exist");
        }
    }
}
