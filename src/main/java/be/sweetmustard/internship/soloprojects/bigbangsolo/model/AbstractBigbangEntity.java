package be.sweetmustard.internship.soloprojects.bigbangsolo.model;

import javax.persistence.*;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractBigbangEntity {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
