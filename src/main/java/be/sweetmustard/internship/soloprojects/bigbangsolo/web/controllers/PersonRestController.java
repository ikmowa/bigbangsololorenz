package be.sweetmustard.internship.soloprojects.bigbangsolo.web.controllers;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Person;
import be.sweetmustard.internship.soloprojects.bigbangsolo.service.PersonService;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.controllers.mappers.PersonMapper;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.PersonDto;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.validation.PostValidation;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.validation.PutValidation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/person")
public class PersonRestController {

    private final PersonService service;
    private final PersonMapper personMapper;

    public PersonRestController(final PersonService service, final PersonMapper personMapper) {
        this.service = service;
        this.personMapper = personMapper;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonDto> getPerson(@PathVariable("id")final UUID id) {
        return service.findPersonById(id)
                .map(personMapper::mapPersonToDto)
                .map(dto -> new ResponseEntity<>(dto, HttpStatus.OK))
                .orElseGet(()-> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PersonDto> getAllPersons() {
        return personMapper.mapPersonsToDtos(service.findAllActivePersons());
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonDto> postPerson(@RequestBody @Validated(PostValidation.class) final PersonDto dto) {
        Person saved = service.save(personMapper.mapDtoToPerson(dto));
        return new ResponseEntity<>(personMapper.mapPersonToDto(saved), HttpStatus.CREATED);
    }

    @PutMapping(value = "/")
    public ResponseEntity<PersonDto> putPerson(@RequestBody @Validated(PutValidation.class) final PersonDto personDto) {
        Person updated = service.update(personMapper.mapDtoToPerson(personDto));
        return new ResponseEntity<>(personMapper.mapPersonToDto(updated), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<PersonDto> deletePerson(@PathVariable("id") final UUID id) {
        service.removePerson(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
