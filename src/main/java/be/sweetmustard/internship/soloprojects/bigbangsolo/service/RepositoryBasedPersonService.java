package be.sweetmustard.internship.soloprojects.bigbangsolo.service;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Person;
import be.sweetmustard.internship.soloprojects.bigbangsolo.repositories.PersonRepository;
import be.sweetmustard.internship.soloprojects.bigbangsolo.service.exceptions.PersonServiceException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RepositoryBasedPersonService implements PersonService{
    private final PersonRepository repository;

    public RepositoryBasedPersonService(final PersonRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Person> findPersonById(final UUID id) {
        return repository.findByIdAndActiveIsTrue(id);
    }

    @Override
    public List<Person> findAllActivePersons() {
        return repository.findAllByActiveIsTrue();
    }

    @Override
    public Person save(final Person person) {
        person.setActive(Boolean.TRUE);
        return repository.save(person);
    }

    @Override
    public Person update(final Person person) {
        Optional<Person> perhapsAPerson = repository.findByIdAndActiveIsTrue(person.getId());
        if (perhapsAPerson.isPresent()) {
            person.setActive(perhapsAPerson.get().getActive());
            return repository.save(person);
        } else {
            throw new PersonServiceException("The dude/chick you want to update does not exist");
        }
    }

    @Override
    public void removePerson(final UUID id) {
        Optional<Person> perhapsAPerson = repository.findByIdAndActiveIsTrue(id);
        if (perhapsAPerson.isPresent()) {
            Person person = perhapsAPerson.get();
            person.setActive(false);
             repository.save(person);
        } else {
            throw new PersonServiceException("The dude/chick you want to delete does not exist");
        }
    }
}
