package be.sweetmustard.internship.soloprojects.bigbangsolo.model;

import javax.persistence.*;

@Entity
public class Part extends AbstractBigbangEntity {
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String bio;

    @Column(nullable = false)
    private Integer startingSeason;

    @ManyToOne(optional = false)
    private Serie serie;

    @ManyToOne(optional = false)
    private Person person;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Integer getStartingSeason() {
        return startingSeason;
    }

    public void setStartingSeason(Integer startingSeason) {
        this.startingSeason = startingSeason;
    }

    public Serie getSerie() {
        return serie;
    }

    public void setSerie(Serie serie) {
        this.serie = serie;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
