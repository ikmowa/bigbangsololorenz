package be.sweetmustard.internship.soloprojects.bigbangsolo.web.controllers;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Person;
import be.sweetmustard.internship.soloprojects.bigbangsolo.service.PersonService;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.controllers.mappers.PersonMapper;
import be.sweetmustard.internship.soloprojects.bigbangsolo.web.model.PersonDto;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;

@Controller
@RequestMapping("/person")
public class PersonThymeleafController {

    private final PersonService personService;
    private final PersonMapper personMapper;

    public PersonThymeleafController(final PersonService personService, final PersonMapper personMapper) {
        this.personService = personService;
        this.personMapper = personMapper;
    }

    @GetMapping(value = "/", produces = {MediaType.TEXT_HTML_VALUE})
    public String presentPersonList(Model model) {
        model.addAttribute("persons", personMapper.mapPersonsToDtos(personService.findAllActivePersons()));
        return "person/list";
    }

    @GetMapping(value = "/{id}", produces = {MediaType.TEXT_HTML_VALUE})
    public String presentPersonDetail(Model model, @PathVariable("id") UUID id) {
        model.addAttribute("person", personService
                .findPersonById(id)
                .map(personMapper::mapPersonToDto)
                .orElse(null));
        return "person/detail";
    }

    @PostMapping(value = "/", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.TEXT_HTML_VALUE})
    public String postPerson(@ModelAttribute PersonDto dto, Model model) {
        if (dto.getId() == null) {
            Person saved = personService.save(personMapper.mapDtoToPerson(dto));
            personMapper.mapPersonToDto(saved);
        } else {
            Person updated = personService.update(personMapper.mapDtoToPerson(dto));
            personMapper.mapPersonToDto(updated);
        }
        return "redirect:/person/" + dto.getId();
    }
}