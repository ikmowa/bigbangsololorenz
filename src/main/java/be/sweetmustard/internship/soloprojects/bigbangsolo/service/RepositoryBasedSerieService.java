package be.sweetmustard.internship.soloprojects.bigbangsolo.service;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Serie;
import be.sweetmustard.internship.soloprojects.bigbangsolo.repositories.SerieRepository;
import be.sweetmustard.internship.soloprojects.bigbangsolo.service.exceptions.SerieServiceException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RepositoryBasedSerieService implements SerieService {

    private final SerieRepository repository;

    public RepositoryBasedSerieService(final SerieRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Serie> findById(final UUID id) {
        return repository.findById(id);
    }

    @Override
    public List<Serie> findAllSeries() {
        return repository.findAll();
    }

    @Override
    public Serie save(final Serie serie) {
        return repository.save(serie);
    }

    @Override
    public Serie update(final Serie serie) {
        Optional<Serie> foundSerie = repository.findById(serie.getId());
        if (foundSerie.isPresent()) {
            return repository.save(serie);
        } else {
            throw new SerieServiceException("The serie you tried to update does not exist");
        }
    }

    @Override
    public void delete(final UUID id) {
        Optional<Serie> perhapsASerie = repository.findById(id);
        if (perhapsASerie.isPresent()) {
            Serie serie = perhapsASerie.get();
            repository.delete(serie);
        } else {
            throw new SerieServiceException("The serie you tried to delete does not exist");
        }
    }
}
