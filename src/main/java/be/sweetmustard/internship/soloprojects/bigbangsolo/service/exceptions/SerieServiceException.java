package be.sweetmustard.internship.soloprojects.bigbangsolo.service.exceptions;

public class SerieServiceException extends RuntimeException {
    public SerieServiceException() {
    }

    public SerieServiceException(String message) {
        super(message);
    }

    public SerieServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public SerieServiceException(Throwable cause) {
        super(cause);
    }

    protected SerieServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
