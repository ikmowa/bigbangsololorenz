package be.sweetmustard.internship.soloprojects.bigbangsolo.service;

import be.sweetmustard.internship.soloprojects.bigbangsolo.model.Person;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PersonService {
    Optional<Person> findPersonById(UUID id);

    List<Person> findAllActivePersons();

    Person save(Person person);

    Person update(Person person);

    void removePerson(UUID id);
}
